# STEP_BUILD = $(shell sh ci/build/build.sh)
# VERSION_TAG = $(shell sh ci/get-tag.sh)
# UNIT_TEST = $(shell sh ci/unit_test.sh)
# BRANCH_NAME = $(shell echo "$$(git rev-parse --abbrev-ref HEAD)") #branch name
# VERSION_LATEST = $(shell echo "$$(git rev-parse --abbrev-ref HEAD)") #branch name

.PHONY: help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

###CICD
build: ## Build the docker image
	@echo "Build Step........................................................................"
	./ci/build/build.sh
	@echo ".................................................................................."
	
test: ## Test the docker image
	@echo "Test Step........................................................................"
	./ci/test/test.sh
	@echo ".................................................................................."

release: ## Release the docker image to docker repository
	@echo "Release Step........................................................................"
	./ci/release/release.sh
	@echo ".................................................................................."

deploy: ## Deploy to environtment
	@echo "Deploy to environtment........................................................................"
	./cd/cd.sh
	@echo ".................................................................................."

# delovery: ## Delivery to environtment
# 	@echo "delovery......"

provision: ## Deploy to environtment
	@echo "Provision to environtment........................................................................"
	./ci/script/provision.sh
	@echo ".................................................................................."


###OPERATE
run: ## run container
	@echo "docker run......"

stop: ## stop container
	@echo "docker stop......"

rm: ## remove container
	@echo "docker remove......"


