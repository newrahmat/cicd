source ci/script/setup.sh
source ci/script/git.sh

echo '====>>> DOCKER BUILD ===>>>'
# docker build -t ${DOCKER_IMAGE}:${TAG_LATEST} -t ${DOCKER_IMAGE}:${TAG_VERSION} -f ci/build/Dockerfile .
docker build --build-arg BRANCH=${GIT_BRANCH_NAME} --build-arg PORT=${APP_PORT}  -t ${DOCKER_IMAGE}:${TAG_LATEST} -t ${DOCKER_IMAGE}:${TAG_VERSION} -f ci/build/docker/${DOCKER_TEMPLATE}/Dockerfile .

echo '===== DOCKER IMAGE LIST ====='
docker images ${DOCKER_IMAGE}:${TAG_LATEST} && docker images ${DOCKER_IMAGE}:${TAG_VERSION}