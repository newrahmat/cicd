##########
GIT_BRANCH_NAME=$(git rev-parse --abbrev-ref HEAD)
#hardcode
# GIT_BRANCH_NAME="develop"

if [ $GIT_BRANCH_NAME = "master" ]
then
    ENV="pro"
elif [ $GIT_BRANCH_NAME = "staging" ]
then
    ENV="sta"
elif [ $GIT_BRANCH_NAME = "develop" ] 
then
    ENV="dev"
elif [ $GIT_BRANCH_NAME = "development" ] 
then
    ENV="dev"
fi


COMMIT_ID=$(git rev-parse --short HEAD)

REPO_NAME=$(basename `git rev-parse --show-toplevel`)
#hardcode
# REPO_NAME="qoin-be-briva-module"
PROJECT="$( cut -d '-' -f 1 <<< "$REPO_NAME" )"

if [ $PROJECT = "kor" ]
then
    PROJECT="korlantas"
fi

TYPE="$( cut -d '-' -f 2 <<< "$REPO_NAME" )"
NAMESPACE="$( cut -d '-' -f 3 <<< "$REPO_NAME" )"
GET_RULE="$( cut -d '-' -f 4 <<< "$REPO_NAME" )"

if [ "${GET_RULE}" = "manager" ]
then
    RULE="manager"
    NODE_TYPE="front"
elif [ "${GET_RULE}" = "module" ]
then
    RULE="module"
    NODE_TYPE="back"
else
    RULE="general"
    NODE_TYPE="front"
fi


TAG_VERSION=${COMMIT_ID}
TAG_LATEST=${ENV}
DOCKER_REPO=${DOCKER_REPO}
IMAGE_NAME=${REPO_NAME}
DOCKER_IMAGE=${DOCKER_REPO}/${IMAGE_NAME}
# NODE_TYPE=
# echo ${NODE_TYPE}