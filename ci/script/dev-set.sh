source ci/script/setup.sh
# source ci/build/build.sh
source ci/script/git.sh
#set deployment in server dev
echo "===>> provision deployment/${REPO_NAME}"
ssh ${DEV_USER}@${HOST_DEV} << EOF1
mkdir -p deployment/${REPO_NAME}
cat <<"EOF2">deployment/${REPO_NAME}/docker-compose.yaml
version: '2'
services:
  app:
    image: ${DOCKER_IMAGE}:${TAG_VERSION}
    ports:
      - "${APP_PORT}:${APP_PORT}"
    mem_limit: ${MEM_LIMIT}MB
    mem_reservation: ${MEM_REQ}MB
    network_mode: host
    restart: always
EOF2
EOF1

echo "===>>>  ${ENV} environtment ===>>> done........................"
