source ci/script/setup.sh
# source ci/build/build.sh
source ci/script/git.sh

#set deployment in server staging
echo "===>>> provision deployment/${GIT_BRANCH_NAME}-${PROJECT}/${REPO_NAME}"
ssh ${JENKINS_USER}@${HOST_JENKINS} << EOF1
mkdir -p deployment/${GIT_BRANCH_NAME}-${PROJECT}
mkdir -p deployment/${GIT_BRANCH_NAME}-${PROJECT}/${REPO_NAME}
EOF1


ssh ${JENKINS_USER}@${HOST_JENKINS} << EOF2
cat <<"EOF3">deployment/${GIT_BRANCH_NAME}-${PROJECT}/${REPO_NAME}/namespace.yaml
apiVersion: v1
kind: Namespace
metadata:
  name: ${NAMESPACE}
EOF3

cat <<"EOF4">deployment/${GIT_BRANCH_NAME}-${PROJECT}/${REPO_NAME}/deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ${NAMESPACE}-${RULE}
  namespace: ${NAMESPACE}
spec:
  template:
    metadata:
      matchLabels:
        app: ${NAMESPACE}-${RULE}
        project: ${ENV}-${PROJECT}
        role: ${RULE}

spec:
  selector:
    matchLabels:
      app: ${NAMESPACE}-${RULE}
      project: ${ENV}-${PROJECT}
      role: ${RULE}
  replicas: 1
  template:
    metadata:
      labels:
        app: ${NAMESPACE}-${RULE}
        project: ${ENV}-${PROJECT}
        role: ${RULE}      
        
    spec:
      containers:
        - name: ${NAMESPACE}-${RULE}
          image: ${DOCKER_REPO}/${IMAGE_NAME}:${TAG_VERSION}
        nodeSelector:
          nodetype: ${NODE_TYPE}      
#          resources:
#            limits:
#              memory: "${MEM_LIMIT}Mi"
##                cpu: "800m"
#            requests:
#              memory: "${MEM_REQ}Mi"
##                cpu: "400m"
EOF4

cat <<"EOF5">deployment/${GIT_BRANCH_NAME}-${PROJECT}/${REPO_NAME}/service.yaml
apiVersion: v1
kind: Service
metadata:
  name: ${NAMESPACE}-${RULE}
  namespace: ${NAMESPACE}  
spec:
  selector:
    app: ${NAMESPACE}-${RULE}
    project: ${ENV}-${PROJECT}
    role: ${RULE}    
  type: ClusterIP
  ports:
  - protocol: TCP
    port: ${APP_PORT}
    targetPort: ${APP_PORT}
EOF5


cat <<"EOF6">deployment/${GIT_BRANCH_NAME}-${PROJECT}/${REPO_NAME}/service-nodeport.yaml
kind: Service
apiVersion: v1
metadata:
  name: nodeport-${NAMESPACE}-${RULE}
  namespace: ${NAMESPACE}  
spec:
  type: NodePort
  selector:
    app: ${NAMESPACE}-${RULE}
    project: ${ENV}-${PROJECT}
    role: ${RULE}
  ports:
  - port: ${APP_PORT}
    targetPort: ${APP_PORT}
    nodePort: ${APP_PORT}    
    protocol: TCP
    name: http
EOF6
EOF2


echo "===>>>  ${ENV} environtment ===>>> done........................"
