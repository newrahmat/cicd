
source ci/script/setup.sh
source ci/script/git.sh

echo '====>>> DOCKER PUSH ===>>>'
docker push ${DOCKER_IMAGE}:${TAG_LATEST} && docker push ${DOCKER_IMAGE}:${TAG_VERSION} 

echo '===== IMAGE RELEASE ====='

echo "IMAGE TAG LATEST  ---> ${DOCKER_IMAGE}:${TAG_LATEST} " 
echo "IMAGE TAG VERSION ---> ${DOCKER_IMAGE}:${TAG_VERSION} " 
