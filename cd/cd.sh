source ci/script/setup.sh
source ci/script/git.sh
# ENV=${ENV}

if [ ${ENV} = "pro" ]
then
    echo "deploy to production environtment"
elif [ ${ENV} = "stg" ]
then
    echo "deploy to staging environtment"
elif [ ${ENV} = "dev" ] 
then
    echo "deploy to development environtment"
# 
ssh ${DEV_USER}@${HOST_DEV} << EOF1
mkdir -p deployment/${REPO_NAME}
cat <<"EOF2">deployment/${REPO_NAME}/docker-compose.yaml
version: '2'
services:
  app:
    image: ${DOCKER_IMAGE}:${TAG_VERSION}
    ports:
      - "${APP_PORT}:${APP_PORT}"
    mem_limit: ${MEM_LIMIT}MB
    mem_reservation: ${MEM_REQ}MB
    network_mode: host
    restart: always
EOF2
EOF1

ssh ${DEV_USER}@${HOST_DEV} << EOF3
cd deployment/${REPO_NAME}
docker-compose pull && docker-compose up -d --force-recreate
EOF3


# 
else
    echo "deploy to else environtment"
fi